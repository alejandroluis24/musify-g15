class Playlist < ApplicationRecord
  has_many :playlist_songs
  has_many :songs, through: :playlist_songs

  # Sin modelo tabla intermedia
  # has_many_and_belongs_to :songs
  
  belongs_to :user#, optional: true
end
