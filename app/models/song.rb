class Song < ApplicationRecord
  has_many :playlist_songs
  has_many :playlists, through: :playlist_songs

  # Sin modelo tabla intermedia
  # has_and_belongs_to_many :playlists
end
