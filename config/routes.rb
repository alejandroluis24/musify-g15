Rails.application.routes.draw do
  resources :songs do
    member do
      post 'playlists'
    end
  end
  # post 'songs/:id/playlists', to: 'playlist_songs#create'

  # get 'playlists', to: 'playlists#index'
  resources :playlists, only: :index

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
